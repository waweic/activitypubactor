<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Optout - Activitypub.actor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#1da1f2">
    <meta property="og:title" content="Activitypub.actor" />
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css" />
    <link rel="stylesheet" href="/general.css" type="text/css" />
</head>

<body>
    <div class="content">
        <h1>Opt-out from Twitter.ActivityPub.Actor</h1>
        <p>WIP. If you want to block the service for your Twitter username.</p>
        <h2>What does it do?</h2>
        <p>When a user will type your username, it will redirect him/her to a blank page without displaying your username. You just need to follow these small steps.</p>
        <h2>Do it!</h2>
        <p>By using the opt-out function, you consent the website to store your username and a link to your tweet in its database. It is a technical need.</p>
        <h3>1. Tweet this</h3>
        <p>Tweet this text :<br/>
            <pre class="code">@1da1f2 I want to opt-out from TwitterActivityPubActor 🚷</pre>
            <br/>You can use this <a href="https://twitter.com/intent/tweet?text=@1da1f2%20I%20want%20to%20opt-out%20from%20TwitterActivityPubActor%20%F0%9F%9A%B7" target="_blank"><button>Instant tweet button</button></a>
        </p>
        <p>As almost nobody is following @1da1f2, this tweet will not be broadcasted to your followers.</p>
        <h3>2. Copy the link</h3>
        <p>Copy the link of your tweet here : <br/>
            <?php
            if(isset($_GET["link"]) && $_GET["link"]!=""){
            ?>
                <form method="get">
                    <input disabled type="text" name="link" placeholder="https://twitter.com/username/status/000000000000000" style="width:350px" />
                </form>
        </p>
        <p>
            <?php
                $linkexplode=explode("/",$_GET["link"]);
                if($linkexplode[2]!="twitter.com" || $linkexplode[3]==null){
                    ?>
                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Error</strong> : wrong link. <a href="?link"><button>Try again</button></a><br/>
                <?php
                }else{
                    $username=$linkexplode[3];
                    ?>
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i> User <?php echo("@".$username); ?> successfully detected! <br/>
                    <?php
                    $database=file_get_contents("optoutdb.txt");
                    $duplicate=false;
                    foreach(preg_split("/((\r?\n)|(\r\n?))/", $database) as $line){
                        $dbuser=explode(";", $line);
                        if($dbuser[0]==$username){
                            $duplicate=true;
                        }
                    }
                    if($duplicate){
                        ?><i class="fa fa-warning" aria-hidden="true"></i> <?php echo("@".$username); ?> already opt-out! <br/><?php
                    }else{
                        error_reporting(E_ERROR | E_PARSE);
                        $dom = new \PHPHtmlParser\Dom();
                        $dom->loadFromUrl($_GET["link"]);

                        if($dom->find('meta[property="og:description"]')[0]!=null){
                            $tweet=$dom->find('meta[property="og:description"]')[0]->getAttribute("content");
                        }else{
                            $tweet="";
                        }
                        error_reporting(E_ALL);
                        if($tweet=="“@1da1f2 I want to opt-out from TwitterActivityPubActor 🚷”"){
                            ?><i class="fa fa-check-square" aria-hidden="true"></i> Tweet successfully checked! <br/><?php
                            if(file_put_contents("optoutdb.txt",$username.";".$_GET["link"]."\r\n",FILE_APPEND)!=false){
                                ?><i class="fa fa-database" aria-hidden="true"></i> ...and successfully saved in database! <br/><?php
                            }else{
                                ?><i class="fa fa-warning" aria-hidden="true"></i> Error while saving in database. Please contact an admin! <br/><?php
                            }
                        }else{
                            ?><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Error</strong> : wrong tweet. <a href="?link"><button>Try again</button></a><?php
                        }
                    }
                }
            }else{ ?>
                <form method="get">
                    <input type="text" name="link" placeholder="https://twitter.com/username/status/000000000000000" style="width:350px" />
                    <input type="submit" />
                </form>
            <?php } ?>
        </p>
        <h3>3. Keep your tweet</h3>
        <p>Do not delete your tweet, this is a proof!</p>
        <hr>
        <h3>Cancel your opt-out</h3>
        <p>
        <?php
        if(isset($_GET["cancelusername"]) && $_GET["cancelusername"]!=""){
            $username=str_replace("@","",$_GET["cancelusername"]);
            $database=file_get_contents("optoutdb.txt");
            $inDatabase=false;
            foreach(preg_split("/((\r?\n)|(\r\n?))/", $database) as $line){
                $dbuser=explode(";", $line);
                if($dbuser[0]==$username){
                    $inDatabase=true;
                    $tweetlink=$dbuser[1];
                }
            }
            if($inDatabase){
                error_reporting(E_ERROR | E_PARSE);
                $dom = new \PHPHtmlParser\Dom();
                $dom->loadFromUrl($tweetlink);

                if($dom->find('meta[property="og:description"]')[0]!=null){
                    $tweet=$dom->find('meta[property="og:description"]')[0]->getAttribute("content");
                }else{
                    $tweet="";
                }
                error_reporting(E_ALL);
                if($tweet=="“@1da1f2 I want to opt-out from TwitterActivityPubActor 🚷”"){
                    ?><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Error</strong> : the opt-out tweet is still online. You need to delete your tweet first. To find it, search "from:yourUsername TwitterActivityPubActor" on Twitter.com!<?php
                }else{
                    $database=str_replace($username.";".$tweetlink."\r\n","",$database);
                    if(file_put_contents("optoutdb.txt",$database)!=false || strlen($database)==0){
                        ?><i class="fa fa-check-square" aria-hidden="true"></i> You have been successfully removed from opt-out database! <br/><?php
                    }else{
                        ?><i class="fa fa-warning" aria-hidden="true"></i> Error while saving in database. Please contact an admin! <br/><?php
                    }
                }
            }else{
                ?><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <strong>Error</strong> : this username is not in database. Check spelling (case-sentitive). <a href="?tryagain"><button>Try again</button></a><?php
            }
        ?>
        </p>
        <?php }else{ ?>
        <p>If you want to cancel your opt-out, enter your username here :<br/>
            <form method="get">
                <input type="text" name="cancelusername" placeholder="@username" />
                <input type="submit" />
            </form>
        </p>
        <?php } ?>
    </div>
</body>

</html>