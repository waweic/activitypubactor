<?php

$logEnabled=false;
$logMode="verbose";
$serverEnv="dev";

if($serverEnv!="dev"){
    if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit();
    }
}

require("autoload.php");

if(isset($_GET["q"]) && $_GET["q"]!=""){
    $q=$_GET["q"];
}else{
    $q="index";
}

if($logEnabled){
    $headers = apache_request_headers();
    $echolog="";
    foreach ($headers as $header => $value) {
        $echolog.="$header: $value\n";
    }
    $echolog.="Accept: ".$_SERVER["HTTP_ACCEPT"]."\n";
    file_put_contents("log/".date("d-m-Y").".txt",date("h:i:sa")." : ".$_SERVER["REQUEST_URI"]."\r".$echolog."\r",FILE_USE_INCLUDE_PATH | FILE_APPEND);
}

$core=new Core();
$core->welcome($q);

?>