<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@<?php echo($username); ?> - Twitter account - ActivityPub.Actor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#1da1f2">
        <meta property="og:title" content="@<?php echo($username); ?> - Twitter account - Activitypub.actor" />
        <meta property="og:type" content="profile" />
        <meta property="og:url" content="https://twitter.activitypub.actor/<?php echo($username); ?>" />
        <meta property="og:image" content="https://twitter.activitypub.actor/<?php echo($username); ?>/avatar.jpg" />
        <meta property="og:image" content="https://twitter.activitypub.actor/<?php echo($username); ?>/header.jpg" />
        <meta property="og:profile:username" content="<?php echo($username); ?>" />
        <meta property="og:profile:profile:first_name" content="<?php echo($name); ?>" />
        <meta property="og:profile:profile:last_name" content="" />
        <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css"/>
        <link rel="stylesheet" href="/general.css" type="text/css"/>
        <link rel="stylesheet" href="/account.css" type="text/css"/>
    </head>
    <body>
        <div class="content">
            <header>
                <div class="banner">
                    <img src="/<?php echo($username); ?>/header.jpg"/>
                </div>
                <div class="footer">
                    <img class="avatar" src="/<?php echo($username); ?>/avatar.jpg"/>
                    <h1><?php echo($name); ?></h1>
                    <h2>@<?php echo($username); ?></h2>
                    <p class="description"><?php echo($description); ?></p>
                    <a class="externallink" href="http://twitter.com/<?php echo($username); ?>" rel="noopener noreferrer" title="By clicking on this link, you will be redirected to Twitter.com">See on <i class="fa fa-twitter" aria-hidden="true"></i> Twitter.com <i class="fa fa-external-link" aria-hidden="true"></i></a>
                    <label id="optionmenu">
                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                        <input type="checkbox"/>
                        <ul>
                            <li><a href=""><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a></li>
                            <li><a href="/<?php echo($username); ?>/json"><i class="fa fa-code" aria-hidden="true"></i> See JSON Actor</a></li>
                            <li><a href=""><i class="fa fa-refresh" aria-hidden="true"></i> Flush cache</a></li>
                            <li><a href="/optout/<?php echo($username); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Opt-out</a></li>
                        </ul>
                    </label>
                </div>
            </header>
            <main>
                <p>This account is hosted on <a href="http://twitter.com/" rel="noopener noreferrer" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter.com</a>.</p>
                <p>You can link it with any ActivityPub capable social network by typing <span class="code">@<?php echo($username); ?>@twitter.activitypub.actor</span>.</p>
                <p>You can <a href="/optout">opt-in <i class="fa fa-sign-in" aria-hidden="true"></i></a> to display the username, the description and the images of your Twitter profile!</p>
                <p>If you use this username on Twitter and want to block it on this service, you can <a href="/optout">opt-out <i class="fa fa-sign-out" aria-hidden="true"></i></a>.</p>
                <p>Learn more about <a href="//activitypub.actor">ActivityPub.Actor <i class="fa fa-activitypub" aria-hidden="true"></i></a>.</p>
            </main>
        </div>
    </body>
</html>