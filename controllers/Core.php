<?php
class Core{
    
    protected $subdomain="twitter";
    
    public function welcome($query) {
        /*
        Routing
        @params: $query
        */
        $query=explode("/",$query);
        $qPos=0;
        
        for($q=0; $q<sizeof($query); $q++){
            if($query[$q]==""){
                unset($query[$q]);
            }
        }
        
        if($query[0]==".well-known"){
            if($query[1]=="webfinger"){
                if(isset($_GET["resource"]) && $_GET["resource"]!=""){
                    $this->webfinger($_GET["resource"]);
                }
            }
        }else{
            //user or static
            if(sizeof($query)==1){
                $staticQuery=$query[0];
            }else{
                foreach($query as $q){
                    if(!isset($staticQuery)){
                        $staticQuery=$q;
                    }else{
                        $staticQuery.="-".$q;
                    }
                }
            }
            if(glob("static/".$staticQuery)){ //file_exists ?
                $fileToRender="static/".$staticQuery;
            }else{
                $allFiles=glob("static/".$staticQuery.".*");
                if(sizeof($allFiles)>1){
                    if(glob("static/".$staticQuery.".php")){
                        $fileToRender="static/".$staticQuery.".php";
                    }elseif(glob("static/".$staticQuery.".html")){
                        $fileToRender="static/".$staticQuery.".html";
                    }else{
                        $fileToRender=$allFiles[0];
                    }
                }elseif(sizeof($allFiles)==1){
                    $fileToRender=$allFiles[0];
                }
            }
            if(isset($fileToRender)){//if file=static
                ob_start();
                header("Content-type: ".mime_content_type_modified ($fileToRender));
                include $fileToRender;
                echo(ob_get_clean()); //render PHP
            }else{//user
                $username=str_replace(array("@","<",">"),"",$query[0]);
                
                $optoutdb=file_get_contents("optoutdb.txt");
                $inoptoutdb=false;
                foreach(preg_split("/((\r?\n)|(\r\n?))/", $optoutdb) as $line){
                    $dbuser=explode(";", $line);
                    if($dbuser[0]==$username){
                        $inoptoutdb=true;
                    }
                }
                
                $optindb=file_get_contents("optindb.txt");
                $inoptindb=false;
                foreach(preg_split("/((\r?\n)|(\r\n?))/", $optindb) as $line){
                    $dbuser=explode(";", $line);
                    if($dbuser[0]==$username){
                        $inoptindb=true;
                    }
                }
                
                if($inoptoutdb){
                    exit("This user opted out from this service.");
                }else if($inoptindb){
                    if(file_exists("cache/data/".$username.".json") && filemtime("cache/data/".$username.".json")>strtotime('-1 Week')){
                        $data = json_decode(file_get_contents("cache/data/".$username.".json"));
                        $avatarSrc=$data->avatarSrc;
                        $headerSrc=$data->headerSrc;
                        $name=$data->name;
                        $description=$data->description;
                    }else{
                        error_reporting(E_ERROR | E_PARSE);
                        $dom = new \PHPHtmlParser\Dom();
                        $dom->loadFromUrl("http://twitter.com/".$username);

                        $avatarSrc=$dom->find(".ProfileAvatar-image")[0]->getAttribute("src");
                        $headerSrc=$dom->find(".ProfileCanopy-headerBg img")[0]->getAttribute("src");
                        $name=str_replace("&nbsp;","",$dom->find(".ProfileHeaderCard-nameLink")[0]->text(true));
                        $description=str_replace("&nbsp;","",$dom->find(".ProfileHeaderCard-bio")[0]->text(true));
                        error_reporting(E_ALL);

                        //store to json
                        $json=json_encode(array(
                            "username" => $username,
                            "name" => $name,
                            "description" => $description,
                            "avatarSrc" => $avatarSrc,
                            "headerSrc" => $headerSrc,
                        ));
                        file_put_contents("cache/data/".$username.".json",$json,FILE_USE_INCLUDE_PATH);
                    }
                }else{
                    $avatarSrc="static/link.jpg";
                    $headerSrc="static/empty.jpg";
                    $name="Twitter link";
                    $description="This is a link to a Twitter profil. You can't interact with it neither follow it.";
                }
                
                if(strpos($_SERVER["HTTP_ACCEPT"], "application/activity+json")!==false || strpos($_SERVER["HTTP_ACCEPT"], "application/json")!==false || strpos($_SERVER["HTTP_ACCEPT"], "application/ld+json")!==false){
                    $query[1]="json";
                }
                
                if(sizeof($query)==1){
                    ob_start();
                    include "views/account.php";
                    echo(ob_get_clean()); //render PHP
                }elseif(sizeof($query)==2){
                    switch($query[1]){
                        case "json" :
                            $activityjson=array(
                                "@context" => array(
                                    "https://www.w3.org/ns/activitystreams",
                                    "https://w3id.org/security/v1",
                                ),
                                "id" => "https://".$this->subdomain.".activitypub.actor/".$username,
                                "type" => "Service",
                                "following" => "https://".$this->subdomain.".activitypub.actor/".$username."/following",
                                "followers" => "https://".$this->subdomain.".activitypub.actor/".$username."/followers",
                                "inbox" => "https://".$this->subdomain.".activitypub.actor/".$username."/inbox",
                                "outbox" => "https://".$this->subdomain.".activitypub.actor/".$username."/outbox",
                                "preferredUsername" => "".$username,
                                "name" => "".$username,
                                "summary" => "<p>".$description."</p>",
                                "manuallyApprovesFollowers" => false,
                                "url" => "https://".$this->subdomain.".activitypub.actor/".$username,
                                "publicKey" => array(
                                    "id" => "https://".$this->subdomain.".activitypub.actor/".$username."#main-key",
                                    "owner" => "https://".$this->subdomain.".activitypub.actor/".$username,
                                    "publicKeyPem" => "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyJAjDVrhKSGRQaG8Z59E\nHB7Q7c58pYOGmEpzV2Vrboi9A0EOOH7qrmVsnuncPOoxd31z3cwAQyCz+WkXVsG5\npUbvG3XjzUQKXSwOUemg8jCJ7/JHrqCpaSX5f4i028F+eNX8yjuYlj208COk37qz\nR96p2Nvzm+3RSodcYIf0qEi2d0x+cmoDSMSf3K/AdkgKIi5IA02kStOAt1bXnpeA\nhw0bdMGjq+z6B083zfZKi4Ya6s51fh/kV/dB/K4VxNsKaMXUhwk/558x5v43OiuO\ntBP4bbJwJm8txCt2eG3WxoDxbZvRbenp4DK4P6F0JLi42oVRWnGTcTKzt0F3KK4d\njQIDAQAB\n-----END PUBLIC KEY-----\n",
                                ),
                                "manuallyApprovesFollowers"=>true,
                                "attachment"=> array(array(
                                    "type"=>"PropertyValue",
                                    "name"=>"Direct link",
                                    "value"=>"<a href=\"https://twitter.com/".$username."\" rel=\"noopener noreferrer\" >https://twitter.com/".$username."</a>")),
                                "icon" => array(
                                    "type" => "Image",
                                    "mediaType" => "image/jpeg",
                                    "url" => "https://".$this->subdomain.".activitypub.actor/".$username."/avatar.jpg",
                                ),
                                "image" => array(
                                    "type" => "Image",
                                    "mediaType" => "image/jpeg",
                                    "url" => "https://".$this->subdomain.".activitypub.actor/".$username."/header.jpg",
                                ),
                                "endpoints" => array(
                                    "sharedInbox" => "https://".$this->subdomain.".activitypub.actor/".$username."/inbox",
                                ),
                            );
                            header('Content-type: application/activity+json');
                            $this->echoWithLog(json_encode($activityjson,JSON_UNESCAPED_SLASHES));
                            break;
                        case "avatar.jpg" :
                            if($inoptindb){
                                if(file_exists("cache/avatar/".$username.".jpg") && filemtime("cache/avatar/".$username.".jpg")>strtotime('-1 Week')){
                                    $image = file_get_contents("cache/avatar/".$username.".jpg");
                                }else{
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $avatarSrc);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                    $image = curl_exec($ch);
                                    curl_close($ch);
                                    file_put_contents("cache/avatar/".$username.".jpg",$image, FILE_USE_INCLUDE_PATH);
                                    $image = file_get_contents("cache/avatar/".$username.".jpg");
                                }
                            }else{
                                $image = file_get_contents("static/link.jpg");
                            }

                            header("Content-type: image/jpeg;charset=utf-8");
                            echo($image);
                            break;
                        case "header.jpg" :
                            if($inoptindb){
                                if(file_exists("cache/header/".$username.".jpg") && filemtime("cache/header/".$username.".jpg")>strtotime('-1 Week')){
                                    $image = file_get_contents("cache/header/".$username.".jpg");
                                }else{
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $headerSrc);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                    $image = curl_exec($ch);
                                    curl_close($ch);
                                    file_put_contents("cache/header/".$username.".jpg",$image, FILE_USE_INCLUDE_PATH);
                                    $image = file_get_contents("cache/header/".$username.".jpg");
                                }
                            }else{
                                $image = file_get_contents("static/empty.jpg");
                            }
                            header("Content-type: image/jpeg;charset=utf-8");
                            echo($image);
                            break;
                        case "inbox":
                        case "outbox":
                        case "following":
                        case "followers":
                            $activityjson=array(
                                "@context" => array(
                                    "https://www.w3.org/ns/activitystreams",
                                ),
                                "id" => "https://".$this->subdomain.".activitypub.actor/".$username,
                                "type" => "OrderedCollection",
                                "totalItems" => 0,
                            );
                            header('Content-type: application/activity+json');
                            $this->echoWithLog(json_encode($activityjson,JSON_UNESCAPED_SLASHES));
                            break;
                    }
                }
            }
        }
    }
    
    public static function error($code=200,$message="") {
        /*
        Give error page
        @params: $code, $message
        */
        if($code!=200 && $message!=""){
            http_response_code($code);
            echo("Error ".$code.": ".$message);
        }else if($code!=200){
            http_response_code($code);
            echo("Error ".$code);
        }else{
            echo("Error : ".$message);
        }
        exit;
    }
    
    public function get($var){
        /*
        Getter
        @params: $var
        */
        return($this->$var);
    }
    
    
    public function webfinger($query){
        /*
        Webfinger
        @params: $query
        */
        $query=explode(":",$query);
        if($query[0]=="acct"){
            $account=explode("@",$query[1]);
            $username=$account[0];
            $domain=$account[1];
            if($domain==$this->subdomain.".activitypub.actor"){
                header("Content-type: application/json;charset=utf-8");
                $this->echoWithLog(json_encode(array(
                    "subject" => join(":", $query),
                    "links" => array(array(
                        "rel" => "self",
                        "href" => "https://".$this->subdomain.".activitypub.actor/".$username,
                        "type" => "application/activity+json",
                    )),
                    "aliases" => array(
                        "https://".$this->subdomain.".activitypub.actor/".$username,
                    ),
                ),JSON_UNESCAPED_SLASHES));
            }else{
                $this->error("404");
            }
        }
    }
    
    
    public function echoWithLog($string){
        echo($string);
        file_put_contents("log/".date("d-m-Y").".txt",date("h:i:sa")." : ".$_SERVER["REQUEST_URI"]."\r".$string."\r",FILE_USE_INCLUDE_PATH | FILE_APPEND);
    }
}